export interface Registro{
	id: number;
	name: string;
	age: number;
	birth: Date;
	subscription: Date;
	cost: number;
}