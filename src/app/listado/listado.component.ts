import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RegistroService } from '../services/registro.service'
import { Registro } from '../interfaces/registro'
@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
  providers: [RegistroService]
})
export class ListadoComponent  {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name'];

  constructor(private fb: FormBuilder, private registroService: RegistroService) {
    
  }

  ngAfterViewInit(): void {
    
  }

  ngOnInit(){
    this.getAllRegistros()
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  getAllRegistros(): void{
    this.registroService.getRegistros().subscribe(data => {
      
      this.dataSource = new MatTableDataSource<any>(data);
      console.log(this.dataSource.data)
    })

  }

  myForm = this.fb.group({
    name: null,
    age: [null, Validators.required],
    birth: [null, Validators.required],
    subscription: [null, Validators.required],
    cost: null,
  });

  hasUnitNumber = false;

  onSubmit(): void {
    const registro:any = {
      id: 0,
      name: this.myForm.value.name,
      age: this.myForm.value.age,
      birth: this.myForm.value.birth,
      subscription: this.myForm.value.subscription,
      cost: this.myForm.value.cost
    }
    this.registroService.newRegistro(registro).subscribe(data => {
      console.log(data)
      this.getAllRegistros()
    })

  }
}
