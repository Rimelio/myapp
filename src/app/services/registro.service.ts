import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Registro } from '../interfaces/registro'

@Injectable()
export class RegistroService {

  registroUrl = 'http://localhost:8082/api/registro';  // URL to web api

  constructor(private http: HttpClient) { }

  getRegistros(): Observable<Registro[]> {
    return this.http.get<Registro[]>(this.registroUrl)
      /*.pipe(
        catchError(this.handleError('getHeroes', []))
      );*/
  }

  editRegistro(registro: any): Observable<Registro>{
    var req = `?name=${registro.name}&age=${registro.age}&birth=${registro.birth}&subscription=${registro.subscription}&cost=${registro.cost}`
    
    this.registroUrl = req
    const options = {params: new HttpParams()}
    //debugger
    return this.http.put<Registro>(this.registroUrl, options)
  }

  newRegistro(registro: Registro): Observable<Registro>{
    var req = `?name=${registro.name}&age=${registro.age}&birth=${registro.birth}&subscription=${registro.subscription}&cost=${registro.cost}`
    
    this.registroUrl = req
    const options = {params: new HttpParams()}
    //debugger
    return this.http.post<Registro>(this.registroUrl, options)
  }

  deleteRegistro(id: number):Observable<unknown>{
    this.registroUrl = `http://localhost:8082/api/registro?ids=${id}`
    return this.http.delete<unknown>(this.registroUrl)
  }
}